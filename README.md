This is a debian package for a tool that captures time-lapse video and uploads
the videos (incrementally) to GCS.

To build the debian package:

```
debuild
```
