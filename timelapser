#!/bin/bash

# The following variables may be overridden at runtime
RUNTIME_MINUTES=1
SECONDS_PER_FRAME=5
VIDEO_DEVICE=/dev/video0
# Supported resolutions can be found interactively:
# ffmpeg -f v4l2 -list_formats all -i ${VIDEO_DEVICE}
VIDEO_RESOLUTION="1280x720"
SEGMENTS_INTERVAL=1m

GS_PATH="gs://test-videos-01/$(date --rfc-3339=date)"


info() {
  echo "$@" >&2
}

while [ "$#" -gt 0 ]
do
  info "Loading configuration from $1"
  source "$1"
  shift
done


now() {
  date +%s
}

interruptible_sleep() {
  # Sleep in the background to make the sleep interruptible; waiting on a
  # foreground process isn't interruptible, but the wait builtin is.
  sleep $@ &
  wait $!
}

run_capture() {
  local framerate=$(echo "scale=4;1/${SECONDS_PER_FRAME}" | bc)
  local i=0
  local current_time=$(now)
  local end_time=$((${current_time} + (${RUNTIME_MINUTES} * 60)))

  # Run ffmpeg in a loop in case of premature exit/failure, targeting
  # one segment that goes until the end time.
  echo "Starting capture for ${RUNTIME_MINUTES} minutes, until timestamp ${end_time}"
  while [ ${current_time} -lt ${end_time} ]
  do
    ffmpeg -n -loglevel error \
      -f v4l2 -video_size "${VIDEO_RESOLUTION}" -i "${VIDEO_DEVICE}" \
      -t $((${end_time} - ${current_time})) \
      -vf fps="${framerate}" -an -c:v libvpx -b:v 20M -crf 4 "${i}.mkv"
    # Increment file serial number to avoid overwriting old data
    let i=i+1
    current_time=$(now)
  done
}

segment_uploader() {
  while read n; do
    echo Segment trigger "$n"
    for f in *.mkv; do
      info "Do incremental upload of file $f"
      gcs-incremental "$f" "${GS_PATH}"
    done
    info "Segment upload completed (for now)"
  done
}

echo "Running for $RUNTIME_MINUTES minutes"

run_capture & capture_pid=$!

# Run another task to scan and upload segments, which we signal by sending
# a message through a named pipe.
msgpipe=$(mktemp -u)
mkfifo -m 600 "${msgpipe}"
# Run a task that does nothing but holds the pipe open; killing this
# pipe holder will terminate the uploader when it reaches EOF (having processed
# anything that was already put into the FIFO).
sleep infinity >"${msgpipe}" & pipe_holder_pid=$!
segment_uploader <"${msgpipe}" & uploader_pid=$!

# Run a subshell to periodically wake this one up to trigger an upload
# while waiting for the capture to finish. We signal through a pipe in order
# to queue events, ensuring they execute serially and never interleave.
trigger_upload() {
  now > "${msgpipe}"
}
trap trigger_upload USR1
{
  while :; do
    interruptible_sleep ${SEGMENTS_INTERVAL}
    kill -USR1 $$ >/dev/null
  done
} & waker_pid=$!

# Wait for capture to finish
while true; do
  wait ${capture_pid}
  wait_status=$?
  if [ $wait_status -lt 128 ]; then
    echo "capture task exited with status $wait_status"
    break
  fi
done

# Terminate the waker
kill $waker_pid

# Run a final segment upload
trigger_upload

# Terminate the pipe holder to close the write end of msgpipe; wait for the
# uploader to complete then exit.
kill $pipe_holder_pid
wait $pipe_holder_pid
echo "Waiting up to 1 hour for uploader to finish processing.."
(sleep 1h; kill -HUP $$) & wait $uploader_pid
echo "Done!"
